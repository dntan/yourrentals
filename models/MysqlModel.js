var config = require('../config/config.js');
var Db = require('mysql-activerecord');

var pool = new Db.Pool({
    server: config.mysqlHost,
    port	 : config.mysqlPort,
    username: config.mysqlUsername,
    password: config.mysqlPassword,
    database: config.mysqlDatabase
});

var getConnection = function(callback) {
	pool.getNewAdapter(function(db) {
		callback(db);
	});
};

var MysqlModel = function() {
	this.save = function(data, callback) {
		var table = 'user';
		getConnection(function(db) {
			db.insert(table, data, function(err, info) {
				if(err) console.log(err);
				callback(info);
				db.releaseConnection();
			});
		});
	};
};

module.exports = new MysqlModel();