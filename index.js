var express = require('express');
var bodyParser = require('body-parser')

var app = express();
app.set('views', (__dirname + '/public/views'));
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var SendEmail = require('./controllers/SendEmail');
//var MongoModel = require('./models/MongoModel');
var MysqlModel = require('./models/MysqlModel');

app.get('/', function(req, res) {
	res.redirect('/signup');
});

app.get('/signup', function(req, res) {
	res.render('signup');
});

app.post('/signup', function(req, res) {
	var username = req.body.username;
	var email = req.body.email;
	if(username && email) {
		MysqlModel.save({username: username, email:email, datecreated: Math.floor(Date.now()/1000)}, function() {
			SendEmail.send(username, email, function() {
				res.render('signup_success', {username: username});	
			});
		});		
	} else {
		res.render('singup_fail');
	}
});

var server = app.listen(3000, function() {
	console.log("Running ..");
});