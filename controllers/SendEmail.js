var mandrill = require('mandrill-api/mandrill');
var config = require('../config/config');
var mandrill_client = new mandrill.Mandrill(config.mandrillKey);
var SendEmail = function() {
	this.send = function(username, email, callback) {
		var template_name = "test-template";
		var template_content = [{
		        "name": "test-template",
		        "content": "Signup confirm"
		}];
		var message = {
		    "to": [{
		            "email": email,
		            "name": username,
		            "type": "to"
		        }],
		    "merge_vars": [
            {
                "rcpt": email,
                "vars": [
                    {
                        "name": "USER_NAME",
                        "content": username
                    },
                    {
                    	"name": "USER_EMAIL",
                    	"content": email
                    }
                ]
            }
        ],
		};
		mandrill_client.messages.sendTemplate({"template_name": template_name, "template_content": template_content, "message": message}, function(result) {
		    callback();
		}, function(e) {
		    console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
		});
	};
};

module.exports = new SendEmail();