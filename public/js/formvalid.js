$(document).ready(function() {
	$("#signup-form").submit(function(e) {
		var username = $("#username").val().trim();
		var email = $("#email").val().trim();
		$(".error").css('display', 'none');
		var valid = true;
		if(!username) {
			console.log('here');
			$("#username-error").css('display', 'block');
			valid = false;
		}
		if(!email) {
			$("#email-error").css('display', 'block');
			valid = false;
		}
		if(valid)
			return true;
		else 
			return false;
		e.preventDefault();
	});
});
