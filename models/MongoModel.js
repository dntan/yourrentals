var mongoose = require('mongoose');
var config = require('../config/config');

mongoose.connect(config.mongodbUrl, function(err) {
    if (err) throw err;
});
var Schema = mongoose.Schema;
var User = new Schema({
	username : {type: String, require: true},
	email : {type: String, require: true},
	createTime : {type: Date},
});


var Mongo = function() {
	this.saveUser = function(collection, obj, callback) {
		var UserModel = mongoose.model(collection, User);
		var sub = new UserModel();
		sub.username = obj.username;
		sub.email = obj.email;
		sub.save(function(err) {
			if(err) callback(false);
			else callback(true);
		});
	};
};

module.exports = new Mongo();